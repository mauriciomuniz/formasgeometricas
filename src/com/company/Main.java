package com.company;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {

        DefineFormaGeometrica defineForma = new DefineFormaGeometrica();
        SolicitaDados solicitaDados = new SolicitaDados();
        List<Double> listaParametros;
        direcionaCalculo direcionaCalculo = new direcionaCalculo();
        double area;

        System.out.println("Informe a quantidade de lados da forma Geométrica pretendida: ");
        Scanner entrada = new Scanner(System.in);
        int entradaLados = entrada.nextInt();

        String formaGeometrica = defineForma.retornaForma(entradaLados);

        if (formaGeometrica.equals("Forma Geometrica não prevista")) {
            System.out.println("Forma Geometrica não prevista");
        } else {
            listaParametros = solicitaDados.solicitaDados(entradaLados, formaGeometrica);

            area = direcionaCalculo.direcionaCalculo(listaParametros);

            System.out.println("A area do " + formaGeometrica + " é: " + area);
        }

    }
}
