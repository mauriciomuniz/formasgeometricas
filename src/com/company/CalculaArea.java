package com.company;

import java.util.Collections;
import java.util.List;

public class CalculaArea {

    public double calcularArea(List<Double> listaParametros) {
        Collections.sort(listaParametros);

        if (listaParametros.get(0) == listaParametros.get(1)) {
            double areaQuadrado = listaParametros.get(0) * listaParametros.get(0);
            return areaQuadrado;
        } else {
            double areaRetangulo = listaParametros.get(1) * listaParametros.get(2);
            return areaRetangulo;
        }
    }
    public double calcularArea (double lado1, double lado2, double lado3) {
        //validaTriangulo.validaTriangulo(lado1, lado2, lado3);
        double s = (lado1 + lado2 + lado3) / 2;
        double areaTriangulo = Math.sqrt(s * (s - lado1) * (s - lado2) * (s - lado3));
        return areaTriangulo;
    }
    public double calcularArea(double raio) {
        double pi = 3.14;
        return pi*raio*raio;
    }

}
