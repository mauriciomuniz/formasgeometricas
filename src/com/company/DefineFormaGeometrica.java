package com.company;

public class DefineFormaGeometrica {

    public String retornaForma(int quantidadeLados) {
        String formaGeometrica = "";
        if (quantidadeLados == 1) {
            formaGeometrica = "Circulo";
        } else if (quantidadeLados == 3) {
            formaGeometrica = "Triangulo";
        } else if (quantidadeLados == 4) {
            formaGeometrica = "Quadrilátero";
        } else {
            formaGeometrica= "Forma Geometrica não prevista";
        }
        return formaGeometrica;
    }
}
