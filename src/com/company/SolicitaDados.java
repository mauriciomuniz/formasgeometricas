package com.company;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class SolicitaDados {
    List<Double> listaParametros = new ArrayList<Double>();

    public List<Double> solicitaDados(int quantidadeLados, String formaGeometrica) {
        String texto ="";
        if (formaGeometrica.equals("Circulo")) {
            texto = "raio";
        } else {
            texto = "lado";
        }
        for (int i=1;i<=quantidadeLados;i++) {
            System.out.println("Informe o "+texto+ " " +i +" do "+formaGeometrica);
            Scanner entrada = new Scanner(System.in);
            double lado = entrada.nextDouble();
            listaParametros.add(lado);
        }
        return listaParametros;
    }
}
