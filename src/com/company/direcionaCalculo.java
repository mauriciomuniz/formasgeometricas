package com.company;

import java.util.List;

public class direcionaCalculo {
    CalculaArea vamos = new CalculaArea();
    double area = 0.0;
    public double direcionaCalculo(List<Double> listaParametros) {
        int numeroParametros = listaParametros.size();

        switch (numeroParametros) {
            case 1:
                area = vamos.calcularArea(listaParametros.get(0));
                break;
            case 3:
                area = vamos.calcularArea(listaParametros.get(0), listaParametros.get(1), listaParametros.get(2));
                break;
            case 4:
                area = vamos.calcularArea(listaParametros);
                break;
        }
        return area;
    }
}
